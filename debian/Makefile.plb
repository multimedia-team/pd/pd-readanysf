lib.name = readanysf~
readanysf~.class.sources = \
	src/readanysf~.cpp \
	src/ReadMedia.cpp \
	src/FifoAudioFrames.cpp \
	src/FifoVideoFrames.cpp \
	$(empty)

datafiles = \
	$(wildcard *.pd) \
	README \
	screengrab0.40.png \
	$(empty)

datadirs = \
	$(empty)

PKG_CONFIG ?= pkg-config
gmerlincflags = $(shell $(PKG_CONFIG) --cflags gmerlin_avdec gavl)
gmerlinlibs = $(shell $(PKG_CONFIG) --libs gmerlin_avdec gavl)

cflags = $(gmerlincflags)
ldlibs = $(gmerlinlibs)

PDLIBBUILDER_DIR=/usr/share/pd-lib-builder
include $(PDLIBBUILDER_DIR)/Makefile.pdlibbuilder
